**DIPLOMATURA EN CIENCIA DE DATOS, APRENDIZAJE AUTOMÁTICO Y SUS APLICACIONES 2021**

Este repositorio contiene las actividades realizadas durante la mentoría "**Detección de Objetos en Imágenes**"

**GRUPO 2:**

- Carignano, Hugo Adrián
- Díaz, Federico Raúl
- Villafañe, Roxana Noelia

## Entregable 1:

Correspondiente a la materia: "Análisis y Visualización de datos". 

Nombre del archivo: Entregable_Final_mentoría.ipynb

En este trabajo se realizó una exploración de los datasets:
* `dataSet_train_crop_features_labels.csv`
* `dataSet_test_crop_features_labels.csv`


Se espera que estos datasets en el futuro sean los sets para entrenar y testear modelos supervisados / no supervisados. 

Análisis realizados:
#### Primer Parte:
    ¿Cuántas entradas posee el dataset?
    ¿Cuántas clases distintas de imágenes tenemos? Nombres y cantidad de datos
    ¿Podemos hacer una generalización mas amplia? Nombres y cantidad de datos
    ¿Hay valores nulos?
    ¿Qué tipo de variable es cada una?
    ¿Existen outliers en las variables seleccionadas?
    ¿Cómo es la distribución de las variables en el dataset? ¿Se distribuyen de manera normal? ¿Qué implicancias tiene esto?

#### Segunda Parte:
Con los features de la ResNet10

    ¿Habrá algunas mas importantes que otras? Analizen las distribuciones, tanto de filas como de columnas.

En caso de calcular estadísticos, explicar por qué lo hicieron y su significado. Utilizar los gráficos apropiados para mostrar el análisis realizado. Además:

    ¿Entre qué variables existe más correlación?

Utilizar gráficos que permitan visualizar de un modo simple las conclusiones.

La extracción de features en las imágenes fue realizada con el script 'features.py', también presente en la carpeta 'dataset', con una red ResNet101 usando pytorch. Una explicación mas o menos similar a lo realizado pueden encontrarla este link.

    ¿Cuales de estas features son mas relevantes?

Otras preguntas disparadoras importantes:

    ¿Esta balanceado el conjunto de datos? 
    ¿Como se distrubuyen las posiciones de los objetos en las imágenes?
    ¿En que región de las imágenes debería buscar los objetos?
    ¿Puedo utilzar el color de las imagenes croppeadas para determinar la clase de objeto?



## Entregable 2:

Correspondiente a la materia: "	Análisis exploratorio y curación de datos".

Nombre del archivo:

En este trabajo se trabajó con los siguientes dataset: 

* `dataSet_train_crop_features_labels_wError.csv`
* `dataSet_train_crop_features_labels.csv`
* `dataSet_test_crop_features_labels.csv`


Análisis realizados: los análisis realizados intentan contestar estas preguntas. 

##### Primer Parte:
Con los datos: `dataSet_train_crop_features_labels_wError.csv`

    Chequeen los tipos de datos. ¿Se corresponden a lo esperado?
    Tomar porciones aleatorias del dataframe para ver si van descubriendo inconsistencias, vayan notándolas y proponiendo como solucionarlas.
    Cuántas imágenes originales tienen en total?
    Chequeen valores nulos, extremos y duplicados. ¿Qué hacemos con ellos? ¿Podemos eliminarlos?
    Si no los eliminan, ¿qué estrategia utilizarían para imputarlos? Esto serviría para cualquier clase de datos?
    Verificar si los tipos de clase de los objeto son los esperados. Puede haber errores de anotación.

    
#### Segunda Parte:
Con los datos: `dataSet_train_crop_features_labels.csv`y `dataSet_test_crop_features_labels.csv`


    ¿Se puede aplicar One-Hot encodign? ¿A que variables?
    ¿Se observó algún tipo de segos en los datos?¿Como los curaria?
    ¿Como solucionar los outliers?
    ¿A que variables del dataFrame se les puede aplicar PCA? Realizar un analis con las gráficas correspondientes.
    Aplicar power transoformer a las variables que considere. ¿Que resultados se pueden obtener?

Features de la ResNet10

    ¿Toman valores mayores a 1 o negativos? ¿Eso qué implica?

Cada imagen pertence a una secuencia de imágenes determinanda (video),

    ¿Qué videos aportan mas diversidad y cuales son mas de lo mismo?.

![Presentacióón de la primera entrega](video1mentoria.mp4)


![Presentacióón de la segunda entrega](Video2Mentoria.mp4)

